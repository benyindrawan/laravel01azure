@extends('layouts.app');

@section('content');

<div class="w-4/5 m-auto text-center">
    <div class="py-15 border-b border-gray-200">
        <h1 class="text-6xl">
            Blog Posts
        </h1>
    </div>
</div>

<div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
    <div>
        <img src="https://gamestation.co.id/wp-content/uploads/2019/05/Creative-Assembly-Izinkan-Pasien-05.jpg" width="700" alt="">
    </div>
    <div>
        <h2 class="text-gray-700 font-bold text-5xl pb-4">
            Sejarah Tiga Negara
        </h2>
        <span class="text-gray-500">
            By <span class="font-bold italic text-gray-800">Beny Indrawan</span>, 1 day ago
        </span>
        <p class="text-xl text-gray-700 pt-8 pb-10 leading-8 font-weight-lighter">
            Dari Wikipedia bahasa Indonesia, ensiklopedia bebas
            Kisah Tiga Negara (Hanzi: 三國演義, hanyu pinyin: sānguó yǎnyì, Bahasa Inggris: Romance of the Three Kingdoms) adalah sebuah roman berlatar-belakang sejarah dari zaman Dinasti Han dan Tiga Negara. Di kalangan Tionghoa di Indonesia, kisah ini dikenal dengan nama Samkok yang merupakan dialek Hokkian dari sanguo atau tiga negara.

            Sering orang salah kaprah akan perbedaan Kisah Tiga Negara atau Kisah Tiga Kerajaan mengingat terjemahan bahasa Inggris dari roman ini adalah Romance of the Three Kingdoms, namun pada sebenarnya, yang tepat adalah Kisah Tiga Negara mengingat pada klimaks roman ini, ketiga pemimpin yang bertikai; Cao Cao (negeri Wei), Liu Bei (negeri Shu) dan Sun Quan (negeri Wu) masing-masing telah memaklumatkan diri sebagai kaisar dan mengklaim legitimasi sebagai kekaisaran yang mewarisi Dinasti Han yang telah runtuh.
        </p>


    </div>
</div>


@endsection

@extends('layouts.app')

@section('content')
<div class="background-image grid grid-cols-1 m-auto">
    <div class="flex text-gray-100 pt-10">
        <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
            <h1 class="sm:text-white text-5xl uppercase font-bold text-shadow-md pb-14">
                Belajar Bersama Beny Indrawan
            </h1>
            <a href="/blog" class="text-center bg-gray-50 text-gray-700 py-2 px-4 font-bold text-xl uppercase">
                Read More
            </a>
        </div>
    </div>
</div>

<div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
    <div>
        <img src="https://cdn.pixabay.com/photo/2017/06/23/11/49/laptop-2434393_1280.jpg" width="700" alt="">
    </div>
    <div class="m-auto sm:m-auto text-left w-4/5 block">
        <h2 class="text-3xl font-extrabold text-gray-600">
            Mau Belajar Coding Dengan Cepat ?
        </h2>

        <p class="py-8 text-gray-500 text-s">
            Klik halaman berikut untuk mengetahui caranya lebih lanjut
        </p>
        <p class="font-extrabold text-gray-600 text-s pb-9">
            Cara Belajar Coding Menggunakan Metode Paling Sederhana
        </p>

        <a href="/blog" class="uppercase bg-blue-500 text-gray-100 text-s font-extrabold py-3 px-8 rounded-3xl">
            Find Out More
        </a>
    </div>
</div>

<div class="text-center p-15 bg-black text-white">
    <h2 class="text-2xl pb-5 text-l">
        Saya ahli dalam bidang
    </h2>
    <span class="font-extrabold block text-4xl py-1">
        Teknik Komputer dan Jaringan
    </span>
    <span class="font-extrabold block text-4xl py-1">
        Manajemen Projek Perangkat Lunak
    </span>
    <span class="font-extrabold block text-4xl py-1">
        Digital Marketing
    </span>
    <span class="font-extrabold block text-4xl py-1">
        PHP
    </span>
</div>

<div class="text-center py-15">
    <span class="uppercase text-s text-gray-400">
        Pengenalan Tentang Video
    </span>

    <h2 class="text-4xl font-bold py-10">
        Postingan
    </h2>

    <p class="m-auto w-4/5 text-gray-500">
        Video merupakan teknologi pengiriman sinyal
        elektronik dari suatu gambar yang bergerak.
        Aplikasi umum dari sinyal video yaitu seperti
        televisi, namun juga ia bisa juga digunakan dalam
        aplikasi lain di dalam bidang teknik, saintifik, produksi dan juga keamanan. Berdasarkan bahasa, kata video ini berasal dari kata Latin, “Saya lihat”.
    </p>
</div>
<div class="sm:grid grid-cols-2 w-4/5 m-auto">
    <div class="flex bg-yellow-700 text-gray-100 pt-10">
        <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block">
            <span class="uppercase text-xs">
                Kompresi Video
            </span>
            <h3 class="text-xl font-bold py-10">
                Pemampatan video atau kompresi video berhadapan dengan kompresi data video digital.
                Kompresi video dibutuhkan untuk koding data video secara efisien dalam format file video dan
                streaming format video. Kompresi adalah sebuah konversi data ke sebuah format yang lebih kecil,
                biasanya dilakukan sehingga data dapat disimpan atau disalurkan lebih efisien..........
            </h3>

            <a href="" class="uppercase bg-transparent border-2 border-gray-100 text-gray-100 text-xs font-extrabold py-3 px-5 rounded-3xl">
                Find Out More
            </a>
        </div>
    </div>
    <div>
        <img src="https://cdn.pixabay.com/photo/2016/09/21/11/31/youtube-1684601_1280.png" width="700" alt="">
    </div>
</div>
@endsection
